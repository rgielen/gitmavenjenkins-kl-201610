package net.rgielen.gmj;

import org.apache.commons.lang3.StringEscapeUtils;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println(new HelloWorld().sayHello("Rene"));
	}

	String sayHello(String name) {
		return "Hello " + StringEscapeUtils.escapeHtml4(name);
	}

}

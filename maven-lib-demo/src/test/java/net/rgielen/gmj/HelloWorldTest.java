package net.rgielen.gmj;

import static org.junit.Assert.*;
import org.junit.Test;

public class HelloWorldTest {

	@Test
	public void validateSayHelloDoesTheRightThing() throws Exception {
		String helloOutput = new HelloWorld().sayHello("foo");
		assertEquals("Hello foo", helloOutput);
	}
	
}
